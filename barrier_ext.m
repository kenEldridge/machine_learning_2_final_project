    % Input: backtracking line search parameters alpha, beta
% initial point x (two dimensional vector)
% equality constraint coefficients A (1*2 matrix) and b
% initial value of t for the barrier method
% parameter mu for the barrier method
% tolerance epsilon for the barrier method
% tolerance epsnewton for the Newton�s method
% Output: optimal solution xopt (two dimensional vector)
function [xopt, output] = barrier_ext(br_fnct,W,a,x,alpha,beta,the_other_t,mu,epsilon,epsnewton,alpha_ext,a_ext)
%  verify that the initial point x fulfills Ax = b

n = length(x);
n_l = round(n/2);
n_h = n - n_l;
opt_x = zeros(n);
opt_x = opt_x(1);
opt_x(1:n_l) = ones(length(n_l));
opt_x(n_l+1:n) = -ones(length(n_h));
opt_x = opt_x';
output = [];
x_out = x;
cont = true;
primal_old = 100000000000000000;
x_old = 0;
stop = 0;
i = 0;
while cont
    
    % Let's try a first order method
    x = gdunc_ext(br_fnct,alpha,beta,W,x,a,the_other_t,epsnewton,alpha_ext,a_ext);
    
    % This is the unstable second order method that mostly, sorta works.
    %x = newtonunc_for_barrier(br_fnct,alpha,beta,W,x,a,the_other_t,epsnewton);
    
    won = ones(length(x));
    won = won(:,1);
    xp = inv(W + diag(x))*won;
    xp = xp > median(xp);
    xpz = xp;
    
    xp = (xp*2 -1);
    primal = xp'*W*xp + alpha_ext*a_ext'*xp;
    primal_opt = opt_x'*W*opt_x + alpha_ext*a_ext'*opt_x;
    dual = (1/4)*alpha_ext^2*a_ext'*(inv(W+diag(x)))*a_ext + sum(x);
    %if dual < -102
    %    'stop';
    %end
    %fprintf('\n');
    %fprintf('%i\t', (x > median(x)));
    %fprintf('\n');
    [o_rows, ~] = size(output);
    %if o_rows > 1
        %stop = primal > output(o_rows-1,1);
    %end
    cont = (n / the_other_t > epsilon) & ~stop;
    %if ~stop
    %    primal_old = primal;
    %    x_old = x;
    %end
    if cont
       the_other_t = mu * the_other_t;
    end
    if ~stop
        lhs = 0;
        rhs = 0;
        if sum(xpz(1:n_l)) >= n_l/2
            lhs = sum(xpz(1:n_l)) / n_l;
        else
            lhs = (n_l - sum(xpz(1:n_l))) / n_l;
        end
        if sum(xpz(n_l+1:n)) >= n_l/2
            rhs = sum(xpz(n_l+1:n)) / n_l;
        else
            rhs = (n_l - sum(xpz(n_l+1:n))) / n_l;
        end
        accuracy = (lhs+rhs)/2;
        fprintf('Primal = %f, Primal Optimal = %f, Dual = %f, i = %i, n/t = %f, epsilon = %f, t*epsilon = %f, accuracy = %f\n', primal, primal_opt, dual, i, n / the_other_t, epsilon, the_other_t*epsilon,accuracy);
        output = [output; primal, primal_opt, dual, accuracy, (x > median(x))', xp'];
        x_out = xp;
    end
    %cont
    %stop
    i = i + 1;
end
fprintf('%i\t', (x_out > median(x_out)));
fprintf('\n');
fprintf('%i\t', (x > median(x)));
fprintf('\n');
fprintf('%i\t', (xp));
fprintf('\n');
xopt = x;
