﻿/*
Ken Eldridge
for CS690 - ML2
Spring 2017
Final Project
find_gpa_stats.sql: split a cohort into the top and bottom halves.  Calc stats for those halves
March 19th, 2017
*/

with s1 as (
	select
		*
	from
		sis.student_profile_pu s
	where
		s.profile_academic_period = '201610'
		AND profile_firstime_fulltime_ind = 'Y'
		AND profile_admissions_population in ('B', 'SB')
		AND profile_campus_site = 'PWL'
		AND profile_level = 'UG'
), s2 as (
	select
		t.*
		, (row_number() over(order by gpa desc))::numeric
		, (count(*) over())::numeric
		, avg(gpa) over()
	from
		s1
	inner join
		sis.frz_gpa_by_term t
	on
		(s1.person_uid = t.person_uid
		and t.gpa_type = 'I'
		and freeze_event = 'TERM_END'
		and t.academic_period = '201610')
), s3 as (
	select
		round(avg(gpa),4) as avg
		, round(stddev(gpa),4) as stddev
		, 'low'::text as grp
	from
		s2
	where
		s2.row_number > (s2.count / 2)
), s4 as (
	select
		round(avg(gpa),4) as avg
		, round(stddev(gpa),4) as stddev
		, 'high'::text as grp
	from
		s2
	where
		row_number <= (count / 2)
), s5 as (
	select
		round(avg(gpa),4) as avg
		, round(stddev(gpa),4) as stddev
		, 'population'::text as grp
	from
		s2
)
select
	grp
	, avg
	, stddev
from
	s3
union
select
	grp
	, avg
	, stddev
from
	s4
union
select
	grp
	, avg
	, stddev
from
	s5
;

-- grp,avg,stddev
-- "low";2.4903;0.6945
-- "high";3.6801;0.2356
