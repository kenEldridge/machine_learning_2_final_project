%{
Ken Eldridge
for CS690 - ML2
Spring 2017
Final Project
load_data.m: Load your data
March 20th, 2017
%}
function [W, a, x] = load_data(dfile)

%Windows
W = csvread(['\\Client\C$\cygwin64\home\kjeldrid\machine_learning_2_final_project\data\mock\' dfile '_matrix.csv']);
a = csvread(['\\Client\C$\cygwin64\home\kjeldrid\machine_learning_2_final_project\data\mock\' dfile '_gpas.csv']);

%Mac
%W = csvread(['\\Client\C$\Users\keneldridge\Workspace\machine_learning_2_final_project\data\mock\' dfile '_matrix.csv']);
%a = csvread(['\\Client\C$\Users\keneldridge\Workspace\machine_learning_2_final_project\data\mock\' dfile '_gpas.csv']);

%x = randi([0,1],length(a));
%x = x(1,:)';
%for idx = 1:numel(x)
%    if x(idx) == 0
%        x(idx) = -1;
%    end
%end

n = length(a);
x = ones(n);
x = x(:,1);
%x(n/2+1:n) = -x(n/2+1:n);

