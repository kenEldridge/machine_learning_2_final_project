% Input: two dimensional vector x
% Output: fx = f(x), gfx = gradient of f at x, Hfx = Hessian of f at x
% Example on how to call the function: [fx gfx Hfx] = f([-4, -3])
function [bv, gbv, Hbv] = br_ext(W,v,the_other_t,alpha_ext,a_ext)

one_vec    = ones(length(v));
one_vec    = one_vec(1,:);
S           = diag(v) + W;
%two-way part standard
%bv          = the_other_t*one_vec*v - log(det( S ));
%extension
bv          = -the_other_t*(1/4)*alpha_ext^2*a_ext'*(inv(W+diag(v))) + one_vec*v - log(det( S ));

persistent E El len;
if (isempty(E) || isempty(El) || len)
    len = length(v);
    E = zeros(length(v));
    El = [];
    for i = 1:length(E)
        M = E;
        M(i,i) = 1;
        M = v(i) * M;
        El = [El M];
    end
end

if det( S ) == 0
    gbv = Inf(length(v));
    Hbv = gbv;
    gbv = gbv(:,1);
else
    
    S_inv = inv(S);
    gbv = zeros(length(v));
    gbv = gbv(:,1);
    Hbv = ones(length(v));
    % g_i = t*1 + tr(inv(S) * E_i)
    for i = 1:length(v)
        %two-way part standard
        %gbv(i)  = the_other_t + trace(S_inv*El(:,(i-1)*len + 1:i*len));
        %extension
        gbv(i)  = the_other_t*(1/4)*alpha_ext^2*a_ext'*(inv(W+diag(v)))^2*a_ext + 1 - trace(S_inv*El(:,(i-1)*len + 1:i*len));
        %for j = 1:length(v)
            % or skip E construct Hbv(i,j) = (S_inv(i,j))^2
        %    Hbv(i,j) = trace(S_inv*El(:,(i-1)*len + 1:i*len)*S_inv*El(:,(j-1)*len + 1:j*len));
            %Hbv(i,j) = (S_inv(i,j))^2;
        %end
    end
end
