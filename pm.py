import datetime
# Q: We have series of timestamps events of when users log in to their accounts.
#    We want to know when users log into their account 5 or more times within any
#    30 day period based on the timestamps of the events.
#
whale_login_events = [
    1490819057.137753,
    1491596657.137753,
    1495225457.137753,
    1495398257.137753,
    1495830257.137753,
    1496003057.137753,
    1496089457.137753,
    1496607857.137753,
    1500236657.137753
]

non_whale_login_events = [
    1490819057.137753,
    1491683057.137753,
    1492979057.137753,
    1493151857.137753,
    1494447857.137753,
    1496089457.137753,
    1496953457.137753,
    1497385457.137753,
    1503606257.137753
]


def has_five_or_more_in_30_days(event_ts):
    """Is this a whale?

    Args:
        event_ts: Unix time stamps in seconds of login events.

    Returns:
        True if there are 5 or more logins during any 30 day period.
    """
    rdt = [datetime.datetime.fromtimestamp(ts) for ts in event_ts]
    rdt.sort()

    w = [(d, d + datetime.timedelta(days=30)) for d in rdt]

    c = max([sum([1 if x[0] <= d and x[1] >= d else 0 for d in rdt]) for x in w])
    return c >= 5


assert not has_five_or_more_in_30_days(non_whale_login_events)

assert has_five_or_more_in_30_days(whale_login_events)


