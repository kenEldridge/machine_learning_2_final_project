% Input: x (two dimensional vector)
% dx (two dimensional vector)
% alpha, beta
% Output: t
function t = btls_ext(fctn,x,dx,alpha,beta,W,the_other_t,alpha_ext,a_ext)
t = 1;
xn = x + t * dx;
[fx_p_t , ~, ~] = fctn(W,xn,the_other_t,alpha_ext,a_ext);
[fx , gfx, ~] = fctn(W,x,the_other_t,alpha_ext,a_ext);

while fx_p_t > fx + alpha * t * gfx' * dx
    
    [fx , gfx, ~] = fctn(W,x,the_other_t,alpha_ext,a_ext);
    t = beta * t;
    x = x + t * dx;
    [fx_p_t , ~, ~] = fctn(W,x,the_other_t,alpha_ext,a_ext);
    

end

