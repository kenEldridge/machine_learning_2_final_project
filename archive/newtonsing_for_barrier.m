% Input: backtracking line search parameters alpha, beta
% initial point x (two dimensional vector)
% equality constraint coefficients A (1*2 matrix) and b
% tolerance epsilon
% Output: optimal solution xopt (two dimensional vector)
function xopt = newtonsing_for_barrier(br_fnct,alpha,beta,W,x,a,the_other_t,epsnewton)

% initialize
v       = 0;
A   = zeros(length(x));
A   = A(1,:);
b   = 0;
[~ , gfx, Hfx] = br_fnct(W,x,the_other_t);
r_dual  = gfx' + A*v;
r_pri   = A*x - b;
r = [r_dual'; r_pri];
r_2_norm = r'*r;
stop    = false;

while ~stop
    
    % KKT matrix
    %kktm    = [Hfx,A';A,0];
    Q = [1];
    h = 0;
    kktm = [Hfx,A';A,0];
    % feasibility vector
    fv      = [-gfx;-h];
    % compute newton steps (primal, dual)
    xnt_w   = linsolve(kktm,fv)';
    % primal
    xnt     = xnt_w(1:2)';
    % dual
    w       = xnt_w(3)';
    vnt     = w - v;
    % compute newton decrement
    t       = 1;
    cont_bk = true;
    while cont_bk
        [~ , gfx, Hfx] = br_fnct(W,a,x + t * xnt,the_other_t);
        % calc new dual residual
        rn_dual  = gfx' + A'* (v + t * vnt) ;
        % calc new primal residual
        rn_pri   = A*(x + t * xnt) - b;
        rn = [rn_dual; rn_pri];
        rn_2_norm = rn'*rn;
        
        cont_bk = rn_2_norm > (1 - alpha * t) * r_2_norm;
        if cont_bk
            t = beta * t;
        end
    end
    
    x   = x + t * xnt;
    v   = v + t * vnt;
    r_2_norm = rn_2_norm;
    
    stop = (A*x - b == 0) & (rn_2_norm <= epsilon);

end

xopt = x;
