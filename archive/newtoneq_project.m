%{
Ken Eldridge
for CS690 - ML2
Spring 2017
Final Project
newtoneq_project.m: a solver
March 20th, 2017
%}
function xopt = newtoneq_project(fctn,W,a,x,gamma,alpha,beta,epsilon)

%  skip feasibility check, i'm only feeding feasible starting points
cont = true;
while cont
    [~ , gfx, Hfx] = fctn(W,a,x,gamma);
    % KKT matrix
    kktm    = [Hfx,x;x',0];
    % feasibility vector
    fv      = [-gfx;0];
    % compute newton step
    %xnt_w   = fv'*inv(kktm);
    xnt_w   = linsolve(kktm,fv)';
    xnt     =  xnt_w(1:length(a))';
    % compute newton decrement
    nd = (-gfx'*xnt)^0.5;
    cont = nd / 2 > epsilon;
    if cont
        x   = times(x,sign(xnt));
    end
end

xopt = x;