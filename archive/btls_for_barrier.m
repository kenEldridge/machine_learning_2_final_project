% Input: x (two dimensional vector)
% dx (two dimensional vector)
% alpha, beta
% Output: t
function t = btls_for_barrier(x,dx,alpha,beta,the_other_t)
t = 1;
[fx_p_t , ~, ~] = br(x + t * dx,the_other_t);
[fx , gfx, ~] = br(x,the_other_t);

while (fx_p_t > fx + alpha * t * gfx * dx)
    
    t = beta * t;
    [fx_p_t , ~, ~] = br(x + t * dx,the_other_t);

end

