% Input: backtracking line search parameters alpha, beta
% initial point x (two dimensional vector)
% equality constraint coefficients A (1*2 matrix) and b
% tolerance epsilon
% Output: optimal solution xopt (two dimensional vector)
function xopt = newtoneq_for_barrier(alpha,beta,x,A,b,t,epsilon)

%  verify that the initial point x fulfills Ax = b
if A*x ~= b
    xopt = 'A*x ~= b';
else
    cont = true;
    while cont
        [~ , gfx, Hfx] = br(x,t);
        % KKT matrix
        kktm    = [Hfx,A';A,0];
        % feasibility vector
        fv      = [-gfx';0];
        % compute newton step
        xnt_w   = linsolve(kktm,fv)';
        xnt     = xnt_w(1:2)';
        % compute newton decrement
        nd = (-gfx*xnt)^0.5;
        cont = nd / 2 > epsilon;
        if cont
            bkt = btls_for_barrier(x,xnt,alpha,beta,t);
            x   = x + bkt * xnt;
        end
    end

    xopt = x;
end

