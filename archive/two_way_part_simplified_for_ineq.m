%{
Ken Eldridge
for CS690 - ML2
Spring 2017
Final Project
two_way_part.m: Dual version of two-way partitioning problem
March 20th, 2017
%}
function [fv, gfv, Hfv] = two_way_part_simplified_for_ineq(v)

% function value
fv      = -sum(v);

% make symbols for derivatives
V = sym('v', [1 length(v)]);

% make the function with symbols
f      = -sum(V);

% Calculate gradients
g    	= gradient(f,V);
gfv     = vpa(subs(g,V,v'));

% Calculate Hessians even though they are a zero
h       = hessian(f,V);
Hfv      = vpa(subs(h,V,v'));




