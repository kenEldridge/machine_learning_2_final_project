% Input: backtracking line search parameters alpha, beta
% initial point x (two dimensional vector)
% tolerance epsilon
% Output: optimal solution xopt (two dimensional vector)
function xopt = gdunc_project(fctn,W,a,x,gamma,alpha,beta,epsilon)

cont = true;
while cont
    [~ , gfx, ~] = fctn(W,a,x,gamma);
    % will never terminate because gfx 
    cont = gfx'*gfx > epsilon;
    if cont
        dx  = -gfx';
        t   = btls(fctn,x,dx,alpha,beta,W,a,gamma);
        x   = x + t * dx';
        %x   = times(x,sign(xt));
    end
end
    
xopt = x;