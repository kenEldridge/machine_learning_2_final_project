%{
Ken Eldridge
for CS690 - ML2
Spring 2017
Final Project
two_way_part.m: Dual version of two-way partitioning problem
March 20th, 2017
%}
function [fx gx Hx] = two_way_part(W,a,v,gamma)
one     = ones(length(a));
one     = one(1,:)';

M = W + diag(v);
% positive definite test
%[~,p] = chol(M)

% positive semi-def test (may have precision erros)
post_semi_def = sum(all(diag(eig((M+M')/2)))) >= 0;

if post_semi_def
    fx = -one'*v;
    
    V = sym('v', [1 length(W(1,:))]);
    % taking the transpose of these sys arrays is strange (so flip it)
    f       = V*-one;
    g    	= gradient(f,V);
    gx     = vpa(subs(g,V,v'));

    h       = hessian(f,V);
    Hx     = vpa(subs(h,V,v'));

else
    fx  = -inf;
    z   = zeros(length(a));
    gx  = z(1,:);
    Hx  = z;
end



