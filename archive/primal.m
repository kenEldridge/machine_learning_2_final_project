%{
Ken Eldridge
for CS690 - ML2
Spring 2017
Final Project
primal.m: primal problem, not convex! It runs approx. 50% for n=10
March 20th, 2017
%}
function [px gpx Hpx] = primal(W,a,x,gamma)
px      = x'*W*x + gamma * a' * x;

X = sym('x', [1 length(a)]);
f       = X*W*X' + gamma * a' * X';
g    	= gradient(f,X);
gpx     = vpa(subs(g,X,x'));

h       = hessian(f,X);
Hpx     = vpa(subs(h,X,x'));
