% Input: backtracking line search parameters alpha, beta
% initial point x (two dimensional vector)
% equality constraint coefficients A (1*2 matrix) and b
% tolerance epsilon
% Output: optimal solution xopt (two dimensional vector)
function xopt = newtonunc_for_barrier(br_fnct,alpha,beta,W,x,a,the_other_t,epsnewton)

cont = true;
while cont
    [~ , gfx, Hfx] = br_fnct(W,x,the_other_t);
    % compute newton step
    dx = -inv(Hfx) * gfx';
    % compute newton decrement
    nd = gfx * inv(Hfx) * gfx';
    cont = nd / 2 > epsilon;
    if cont
        t   = btls(x,dx,alpha,beta);
        x   = x + t * dx;
    end
end
    
xopt = x;
