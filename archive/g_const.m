%{
Ken Eldridge
for CS690 - ML2
Spring 2017
Final Project
g_const.m: inequality constraint function 
March 20th, 2017
%}
function [gv, ggv, Hgv] = g_const(W,v)

% function value
gv      = W + diag(v);

% make symbols for derivatives
V       = sym('v', [1 length(v)]);

% make the function with symbols
f       = W + diag(V);

% Calculate gradients
g       = gradient(f,V);
ggv     = vpa(subs(g,V,v'));

% Calculate Hessians
h       = hessian(f,V);
Hgv     = vpa(subs(h,V,v'));







