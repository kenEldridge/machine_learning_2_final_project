% Input: x (two dimensional vector)
% dx (two dimensional vector)
% alpha, beta
% Output: t
function t = btls(fctn,x,dx,alpha,beta,W,the_other_t)
t = 1;
xn = x + t * dx;
[fx_p_t , ~, ~] = fctn(W,xn,the_other_t);
[fx , gfx, ~] = fctn(W,x,the_other_t);

while fx_p_t > fx + alpha * t * gfx' * dx
    
    [fx , gfx, ~] = fctn(W,x,the_other_t);
    t = beta * t;
    x = x + t * dx;
    [fx_p_t , ~, ~] = fctn(W,x,the_other_t);
    

end

