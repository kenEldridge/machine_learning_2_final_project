import pandas as pd
import matplotlib.pyplot as plt


run = '3'
path = 'data/output/'
nm = 'low_noise_n20.csv'
run1 = pd.read_csv(path + nm)
run1.columns = ['primal_value', 'desired_primal_value', 'dual_value']
run1 = run1.iloc[0:10,:]
plt.plot(run1.primal_value)
plt.plot(run1.dual_value)
plt.plot(run1.desired_primal_value)
plt.legend()
plt.xlabel('iterations')
plt.savefig(path + nm + '.png')


# plt.figure(figsize=(10,5))
# plt.title('Just those predicted positive')
# plt.xlabel('threshold')
# plt.plot(rdp.precision)
# plt.plot(rdp.recall)
# plt.xticks([0,10,20,30,40], labels_p)
# plt.legend()
# plt.savefig('positive.png')

print ('done.')
