    % Input: backtracking line search parameters alpha, beta
% initial point x (two dimensional vector)
% equality constraint coefficients A (1*2 matrix) and b
% initial value of t for the barrier method
% parameter mu for the barrier method
% tolerance epsilon for the barrier method
% tolerance epsnewton for the Newton�s method
% Output: optimal solution xopt (two dimensional vector)
function [xopt, output] = barrier(br_fnct,W,a,x,alpha,beta,the_other_t,mu,epsilon,epsnewton)
%  verify that the initial point x fulfills Ax = b

n = length(x);
n_l = round(n/2);
n_h = n - n_l;
opt_x = zeros(n);
opt_x = opt_x(1);
opt_x(1:n_l) = -ones(length(n_l));
opt_x(n_l+1:n) = ones(length(n_h));
opt_x = opt_x';
output = [];
cont = true;
while cont
    %{
        I keep getting infeasible x values. It leads me to believe
        that something is wrong with br.m but I don't know what
        it is.
    %}
    %x = newtoneq_for_barrier(alpha,beta,x,A,b,t,epsnewton);
    %x = newtonsing_for_barrier(br_fnct,alpha,beta,W,x,a,t,epsnewton);
    %x = newtonuncchol_for_barrier(br_fnct,alpha,beta,W,x,a,t,epsnewton);
    x = newtonunc_for_barrier(br_fnct,alpha,beta,W,x,a,the_other_t,epsnewton);
    won = ones(length(x));
    won = won(:,1);
    xp = inv(W + diag(x))*won;
    xp = x > median(x);
    xp = (xp*2 -1);
    primal = xp'*W*xp;
    primal_opt = opt_x'*W*opt_x;
    dual = sum(x);
    fprintf('Primal = %f, Primal Optimal = %f, Dual = %f\n', primal, primal_opt, dual);
    output = [output; primal, primal_opt, dual];
    %fprintf('\n');
    %fprintf('%i\t', (x > median(x)));
    %fprintf('\n');
    cont = 1 / the_other_t > epsilon;
    if cont
       the_other_t = mu * the_other_t;
    end
end
fprintf('%i\t', (x > median(x)));
fprintf('\n');
xopt = x;
