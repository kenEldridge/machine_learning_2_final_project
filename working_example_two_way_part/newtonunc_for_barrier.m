% Input: backtracking line search parameters alpha, beta
% initial point x (two dimensional vector)
% equality constraint coefficients A (1*2 matrix) and b
% tolerance epsilon
% Output: optimal solution xopt (two dimensional vector)
function xopt = newtonunc_for_barrier(br_fnct,alpha,beta,W,x,a,the_other_t,epsnewton)

cont = true;
while cont
    [fx , gfx, Hfx] = br_fnct(W,x,the_other_t);
    % compute newton step
    dx = -inv(Hfx) * gfx;
    % compute newton decrement
    nd = gfx' * inv(Hfx) * gfx;
    fprintf('fx = %f, nd = %f\n', fx, nd);
    cont = nd / 2 > epsnewton;
    if cont
        t   = btls(br_fnct,x,dx,alpha,beta,W,the_other_t);
        xn   = x + t * dx;
        d = x - xn;
        % if we aren't making progress, move along
        if d'*d < epsnewton
            cont = 0;
        end
        x = xn;
        %x   = x / max(x);
    end
end
    
xopt = x;
