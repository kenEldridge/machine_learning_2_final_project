%{
Ken Eldridge
for CS690 - ML2
Spring 2017
Final Project
main.m: the driver file
March 20th, 2017
%}

%Windows
base_out = '\\Client\C$\cygwin64\home\kjeldrid\machine_learning_2_final_project\data\output\'
%Mac
%base_out = '\\Client\C$\Users\keneldridge\Workspace\machine_learning_2_final_project\data\output\';
alpha       = 0.1; %backtracking param
beta        = 0.5; %backtracking param
% controls the rate at which we grow t
mu          = 1.3;
% controls how big we let t become
epsilon     = 0.00000000001; %stopping criterion
the_other_t = 0.3;
epsnewton   = 0.000000000000000001; %stopping criterion for subproblem

% two_way_part
size = '20';
% pull some mock data
noise_level = 'no';
input_file = strcat(noise_level, strcat('_noise_n', size));
[W, a, x]   = load_data(input_file);
[xopt, output]    = barrier(@br,W,a,x,alpha,beta,the_other_t,mu,epsilon,epsnewton);
out_file = strcat(strcat(base_out, input_file),'.csv');
csvwrite(out_file, output)

stop = 0;
