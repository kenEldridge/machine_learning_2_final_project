import pandas as pd
from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join

the_path = 'data/mock'
the_files = [f for f in listdir(the_path) if isfile(join(the_path, f))]
the_csvs = [fn for fn in the_files if fn.endswith('.csv')]
the_csvs.sort()
m = [d for d in the_csvs if d.find('matrix') >= 0]
g = [d for d in the_csvs if d.find('gpas') >= 0]
mg = list(zip(m,g))
the_csv_dfs = [(pd.read_csv(the_path + '/' + pair[0], header=None).reset_index(),pd.read_csv(the_path + '/' + pair[1], header=None).reset_index()) for pair in mg]
the_csv_dfs_m = [pd.merge(df[0],df[1],on='index') for df in the_csv_dfs]
results = []
for i,csv_df in enumerate(the_csv_dfs_m):
    del csv_df['index']

    n = int(csv_df.shape[0])
    n_l = int(csv_df.shape[0] / 2)
    kmeans = KMeans(n_clusters=2, random_state=0).fit(csv_df)
    kmeans.labels_

    p = kmeans.predict(csv_df)

    lhs = 0
    rhs = 0
    if sum(p[0:n_l]) >= n_l / 2:
        lhs = sum(p[0:n_l]) / n_l
    else:
        lhs = (n_l - sum(p[1:n_l])) / n_l

    if sum(p[n_l:n]) >= n_l / 2:
        rhs = sum(p[n_l + 1:n]) / n_l
    else:
        rhs = (n_l - sum(p[n_l:n])) / n_l

    accuracy = (lhs + rhs) / 2;
    print (the_files[i][0:14], '; accuracy = ', accuracy)
    results.append([m[i][0:14], accuracy])

results_df = pd.DataFrame(results)
results_df.columns = ['dataset', 'kmeans_accuracy']
results_df.to_csv(the_path + '/' + 'kmeans_accuracy.csv')
print ('done.')
