% Input: backtracking line search parameters alpha, beta
% initial point x (two dimensional vector)
% tolerance epsilon
% Output: optimal solution xopt (two dimensional vector)
function xopt = gdunc(br_fnct,alpha,beta,W,x,a,the_other_t,epsilon)

%'gdunc iter'
cont = true;
gfx2 = 100000000000000000;
while cont
    [~ , gfx, ~] = br_fnct(W,x,the_other_t);
    gfx2_old = gfx2;
    gfx2 = gfx'*gfx;
    gfx_close = (abs(gfx2 - gfx2_old) / gfx2) < 0.001;
    cont = gfx2 > epsilon;
    if cont & ~gfx_close
        dx  = -gfx;
        t   = btls(br_fnct,x,dx,alpha,beta,W,the_other_t);
        x   = x + t * dx;
    else
        cont = 0;
    end
end
    
xopt = x;