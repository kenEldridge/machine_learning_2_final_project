'''
Ken Eldridge
for CS690 - ML2
Spring 2017
Final Project
mock_data.py: build a mock dataset in the shape of the colocation data
March 19th, 2017
'''

import pandas as pd
import numpy as np
from collections import namedtuple



def mk_data(noise, n):

    n_half  = int(round(n/2,0))
    n       = int(n)

    # pull random GPAs within the distribution of the lower half of students
    low_half = np.random.normal( noise.low_mean, noise.low_std, n_half )
    # min 0, max 4
    low_half = [max(min(4,x),0) for x in low_half]
    # pull random GPAs within the distribution of the top half of students
    high_half = np.random.normal(noise.high_mean, noise.high_std, n_half )
    # min 0, max 4
    high_half = [max(min(4, x), 0) for x in high_half]
    # concatenate into population
    population = low_half + high_half

    # convert GPAs to [-1,0,1] where -1 is below mean - std, 1 is above mean + std
    population = [1 if x > noise.pop_mean + noise.pop_std else
                 -1 if x < noise.pop_mean - noise.pop_std else 0 for x in population]

    # save the population performance labels
    population = pd.Series(population)
    # print(population.as_matrix().transpose())
    population.to_csv('data/mock/' + noise.name + '_n' + str(int(n)) + '_gpas.csv', header=False, index=False)

    # for each i,j in matrix if i and j both gt median value, Wij get 1 with probability pedge
    #   if i,j both come from the same cluster then probability of an edge is pedge
    #   else (1-pedge)
    m = np.zeros((n,n))
    for i, row in enumerate(m):
        for j, value in enumerate(row):
            if i != j:
                if ( i <= n_half -1 and j <= n_half - 1 ) or ( i > n_half - 1 and j > n_half - 1 ):
                    m[i][j] = np.random.choice(2, 1, p=[(1-noise.pedge),noise.pedge])
                    m[i][j] = min(10,max(0,m[i][j] * np.random.normal(5, 0.5)))
                    m[j][i] = m[i][j]
                else:
                    m[i][j] = np.random.choice(2, 1, p=[noise.pedge, (1 - noise.pedge)])
                    m[i][j] = min(10, max(0, m[i][j] * np.random.normal(5, 0.5)))
                    m[j][i] =  m[i][j]
    # print ( m )
    pd.DataFrame(m.max() - m).astype(int).to_csv('data/mock/' + noise.name + '_n' + str(int(n)) + '_matrix.csv', header=False, index=False)


if __name__ == "__main__":

    Noise = namedtuple('Noise', ['name', 'pedge', 'low_mean', 'low_std', 'high_mean', 'high_std', 'pop_mean', 'pop_std'], verbose=True)

    pop_mean, pop_std = 3.0851, 0.7892
    n = 20.0

    no_noise = Noise(name='no_noise', pedge=1, low_mean=2.4903, low_std=0.6945 * 0.1
                     , high_mean=3.6801, high_std=0.2356 * 0.1, pop_mean=pop_mean, pop_std=pop_std * 0.1)
    extra_low_noise = Noise(name='extra_low_noise', pedge=1, low_mean=2.4903, low_std=0.6945 * 1
                            , high_mean=3.6801, high_std=0.2356 * 1, pop_mean=pop_mean, pop_std=pop_std * 1)
    low_noise = Noise(name='low_noise', pedge=0.9, low_mean=2.4903, low_std=0.6945 * 1
                      , high_mean=3.6801, high_std=0.2356 * 1, pop_mean=pop_mean, pop_std=pop_std * 1)
    med_noise = Noise(name='med_noise', pedge=0.7, low_mean=2.4903, low_std=0.6945 * 1.5
                      , high_mean=3.6801, high_std=0.2356 * 1.5, pop_mean=pop_mean, pop_std=pop_std * 1.5)
    high_noise = Noise(name='high_noise', pedge=0.55, low_mean=2.4903, low_std=0.6945 * 2
                       , high_mean=3.6801, high_std=0.2356 * 2, pop_mean=pop_mean, pop_std=pop_std * 2)

    mk_data(no_noise, n)
    mk_data(low_noise, n)
    mk_data(med_noise, n)
    mk_data(high_noise, n)
    print ('done.')

