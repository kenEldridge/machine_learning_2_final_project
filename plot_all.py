import pandas as pd
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join

the_path = 'data/output'
the_files = [f for f in listdir(the_path) if isfile(join(the_path, f))]
the_csvs = [fn for fn in the_files if fn.endswith('.csv')]
the_csv_dfs = [pd.read_csv(the_path + '/' + csv, header=None) for csv in the_csvs]

for i,csv_df in enumerate(the_csv_dfs):
    # try:
    csv_df.columns = ['primal', 'primal_opt', 'dual', 'accuracy'] + \
                 ['x' + str(i) for i in range(0,int((csv_df.shape[1] - 4)/2))] \
                 + ['z' + str(i) for i in range(0,int((csv_df.shape[1] - 4)/2))]
    csv_df = csv_df.reset_index()
    fig = plt.figure(i)
    ax = fig.add_subplot(111)
    plt.plot(csv_df.primal)
    plt.plot(csv_df.primal_opt)
    plt.plot(csv_df.dual)
    m_acc = max(csv_df.accuracy)
    m_acc_row = csv_df[csv_df.accuracy == m_acc].iloc[0]
    m_acc_x = m_acc_row['index']
    m_acc_y = m_acc_row['primal']
    x_offset = -1 if csv_df.shape[0] / 2 < m_acc_x else 1
    print (x_offset)
    ax.annotate('best accuracy=' + str(m_acc), xy=(m_acc_x,m_acc_y)
                , xytext=(m_acc_x + x_offset*10,m_acc_y - 300)
                , arrowprops=dict(facecolor='black', shrink=1),
                )
    plt.legend(loc=0)
    plt.title(the_files[i][0:14])
    plt.xlabel('iterations')
    plt.savefig(the_path + '/' + the_files[i][0:len(the_files[i]) - 4] + '.png')

kmean_comp = pd.read_csv('data/mock/kmeans_accuracy_w_two_way.csv')
fig = plt.figure(len(the_csv_dfs)+1)
plt.plot(kmean_comp.kmeans_accuracy)
plt.plot(kmean_comp.two_way_w_grade_accuracy)
plt.legend(loc=0)
plt.title('kmeans vs. extended two-way partitioning')
plt.xlabel('iterations')
plt.savefig(the_path + '/kmeans_accuracy_w_two_way.png')
print ('done.')
