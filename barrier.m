    % Input: backtracking line search parameters alpha, beta
% initial point x (two dimensional vector)
% equality constraint coefficients A (1*2 matrix) and b
% initial value of t for the barrier method
% parameter mu for the barrier method
% tolerance epsilon for the barrier method
% tolerance epsnewton for the Newton�s method
% Output: optimal solution xopt (two dimensional vector)
function [xopt, output] = barrier(br_fnct,W,a,x,alpha,beta,the_other_t,mu,epsilon,epsnewton)
%  verify that the initial point x fulfills Ax = b

n = length(x);
n_l = round(n/2);
n_h = n - n_l;
opt_x = zeros(n);
opt_x = opt_x(1);
opt_x(1:n_l) = -ones(length(n_l));
opt_x(n_l+1:n) = ones(length(n_h));
opt_x = opt_x';
output = [];
x_out = x;
cont = true;
primal_old = 100000000000000000;
x_old = 0;
stop = 0;
i = 0;
while cont
    
    % Let's try a first order method
    x = gdunc(br_fnct,alpha,beta,W,x,a,the_other_t,epsnewton);
    
    % This is the unstable second order method that mostly, sorta works.
    %x = newtonunc_for_barrier(br_fnct,alpha,beta,W,x,a,the_other_t,epsnewton);
    
    won = ones(length(x));
    won = won(:,1);
    xp = inv(W + diag(x))*won;
    xp = xp > median(xp);
    
    xp = x > median(x);
    
    xp = (xp*2 -1);
    primal = xp'*W*xp;
    primal_opt = opt_x'*W*opt_x;
    dual = sum(x);
    if dual < -102
        'stop';
    end
    %fprintf('\n');
    %fprintf('%i\t', (x > median(x)));
    %fprintf('\n');
    [o_rows, ~] = size(output);
    %if o_rows > 1
    %    stop = primal > output(o_rows-1,1);
    %end
    cont = (n_l / the_other_t > epsilon) & ~stop;
    %if ~stop
    %    primal_old = primal;
    %    x_old = x;
    %end
    if cont
       the_other_t = mu * the_other_t;
    end
    if ~stop
        fprintf('Primal = %f, Primal Optimal = %f, Dual = %f, i = %i, n/t = %f, epsilon = %f, t*epsilon = %f\n', primal, primal_opt, dual, i, n / the_other_t, epsilon, the_other_t*epsilon);
        output = [output; primal, primal_opt, dual];
        x_out = xp;
    end
    %cont
    %stop
    i = i + 1;
end
fprintf('%i\t', (x_out > median(x_out)));
fprintf('\n');
fprintf('%i\t', (x > median(x)));
fprintf('\n');
fprintf('%i\t', (xp));
fprintf('\n');
xopt = x;
